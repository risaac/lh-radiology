/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.orthanc.beans.study;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.openmrs.module.radiology.orthanc.beans.MainDicomTags;
import org.openmrs.module.radiology.orthanc.beans.PatientMainDicomTags;

public class OrthancStudy {
	
	@JsonProperty("ID")
	private String id;
	
	@JsonProperty("IsStable")
	private Boolean isStable;
	
	@JsonProperty("LastUpdate")
	private String lastUpdate;
	
	@JsonProperty("MainDicomTags")
	private MainDicomTags mainDicomTags;
	
	@JsonProperty("ParentPatient")
	private String parentPatient;
	
	@JsonProperty("PatientMainDicomTags")
	private PatientMainDicomTags patientMainDicomTags;
	
	@JsonProperty("Series")
	private List<String> series = null;
	
	@JsonProperty("Type")
	private String type;
	
	@JsonProperty("Modality")
	private String modality;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	@JsonProperty("ID")
	public String getId() {
		return id;
	}
	
	@JsonProperty("ID")
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonProperty("IsStable")
	public Boolean getIsStable() {
		return isStable;
	}
	
	@JsonProperty("IsStable")
	public void setIsStable(Boolean isStable) {
		this.isStable = isStable;
	}
	
	@JsonProperty("LastUpdate")
	public String getLastUpdate() {
		return lastUpdate;
	}
	
	@JsonProperty("LastUpdate")
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	@JsonProperty("MainDicomTags")
	public MainDicomTags getMainDicomTags() {
		return mainDicomTags;
	}
	
	@JsonProperty("MainDicomTags")
	public void setMainDicomTags(MainDicomTags mainDicomTags) {
		this.mainDicomTags = mainDicomTags;
	}
	
	@JsonProperty("ParentPatient")
	public String getParentPatient() {
		return parentPatient;
	}
	
	@JsonProperty("ParentPatient")
	public void setParentPatient(String parentPatient) {
		this.parentPatient = parentPatient;
	}
	
	@JsonProperty("PatientMainDicomTags")
	public PatientMainDicomTags getPatientMainDicomTags() {
		return patientMainDicomTags;
	}
	
	@JsonProperty("PatientMainDicomTags")
	public void setPatientMainDicomTags(PatientMainDicomTags patientMainDicomTags) {
		this.patientMainDicomTags = patientMainDicomTags;
	}
	
	@JsonProperty("Series")
	public List<String> getSeries() {
		return series;
	}
	
	@JsonProperty("Series")
	public void setSeries(List<String> series) {
		this.series = series;
	}
	
	@JsonProperty("Type")
	public String getType() {
		return type;
	}
	
	@JsonProperty("Type")
	public void setType(String type) {
		this.type = type;
	}
	
	@JsonProperty("Modality")
	public String getModality() {
		return modality;
	}
	
	@JsonProperty("Modality")
	public void setModality(String modality) {
		this.modality = modality;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}
	
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	
}
