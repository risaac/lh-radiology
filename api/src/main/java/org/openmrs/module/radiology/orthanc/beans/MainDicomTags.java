/**
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.radiology.orthanc.beans;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

public class MainDicomTags {
	
	@JsonProperty("AccessionNumber")
	private String accessionNumber;
	
	@JsonProperty("InstitutionName")
	private String institutionName;
	
	@JsonProperty("ReferringPhysicianName")
	private String referringPhysicianName;
	
	@JsonProperty("RequestedProcedureDescription")
	private String requestedProcedureDescription;
	
	@JsonProperty("StudyDate")
	private String studyDate;
	
	@JsonProperty("StudyDescription")
	private String studyDescription;
	
	@JsonProperty("StudyID")
	private String studyID;
	
	@JsonProperty("StudyInstanceUID")
	private String studyInstanceUID;
	
	@JsonProperty("StudyTime")
	private String studyTime;
	
	@JsonProperty("Modality")
	private String modality;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	@JsonProperty("AccessionNumber")
	public String getAccessionNumber() {
		return accessionNumber;
	}
	
	@JsonProperty("AccessionNumber")
	public void setAccessionNumber(String accessionNumber) {
		this.accessionNumber = accessionNumber;
	}
	
	@JsonProperty("InstitutionName")
	public String getInstitutionName() {
		return institutionName;
	}
	
	@JsonProperty("InstitutionName")
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	
	@JsonProperty("ReferringPhysicianName")
	public String getReferringPhysicianName() {
		return referringPhysicianName;
	}
	
	@JsonProperty("ReferringPhysicianName")
	public void setReferringPhysicianName(String referringPhysicianName) {
		this.referringPhysicianName = referringPhysicianName;
	}
	
	@JsonProperty("RequestedProcedureDescription")
	public String getRequestedProcedureDescription() {
		return requestedProcedureDescription;
	}
	
	@JsonProperty("RequestedProcedureDescription")
	public void setRequestedProcedureDescription(String requestedProcedureDescription) {
		this.requestedProcedureDescription = requestedProcedureDescription;
	}
	
	@JsonProperty("StudyDate")
	public String getStudyDate() {
		return studyDate;
	}
	
	@JsonProperty("StudyDate")
	public void setStudyDate(String studyDate) {
		this.studyDate = studyDate;
	}
	
	@JsonProperty("StudyDescription")
	public String getStudyDescription() {
		return studyDescription;
	}
	
	@JsonProperty("StudyDescription")
	public void setStudyDescription(String studyDescription) {
		this.studyDescription = studyDescription;
	}
	
	@JsonProperty("StudyID")
	public String getStudyID() {
		return studyID;
	}
	
	@JsonProperty("StudyID")
	public void setStudyID(String studyID) {
		this.studyID = studyID;
	}
	
	@JsonProperty("StudyInstanceUID")
	public String getStudyInstanceUID() {
		return studyInstanceUID;
	}
	
	@JsonProperty("StudyInstanceUID")
	public void setStudyInstanceUID(String studyInstanceUID) {
		this.studyInstanceUID = studyInstanceUID;
	}
	
	@JsonProperty("StudyTime")
	public String getStudyTime() {
		return studyTime;
	}
	
	@JsonProperty("StudyTime")
	public void setStudyTime(String studyTime) {
		this.studyTime = studyTime;
	}
	
	@JsonProperty("Modality")
	public String getModality() {
		return modality;
	}
	
	@JsonProperty("Modality")
	public void setModality(String modality) {
		this.modality = modality;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}
	
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	
}
